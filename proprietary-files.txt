# All unpinned blobs below are extracted from redfin-user-12-SP1A.210812.015

# AndroidAuto
-product/priv-app/AndroidAutoStubPrebuilt/AndroidAutoStubPrebuilt.apk;PRESIGNED

# Arcore
-product/app/arcore/arcore.apk;PRESIGNED

# Browser
-product/app/Chrome-Stub/Chrome-Stub.apk;OVERRIDES=webview,Browser2,Jelly;PRESIGNED
product/app/Chrome/Chrome.apk.gz
-product/app/TrichromeLibrary-Stub/TrichromeLibrary-Stub.apk;PRESIGNED
product/app/TrichromeLibrary/TrichromeLibrary.apk.gz
-product/app/WebViewGoogle-Stub/WebViewGoogle-Stub.apk;OVERRIDES=webview;PRESIGNED
product/app/WebViewGoogle/WebViewGoogle.apk.gz
-product/priv-app/ScribePrebuilt/ScribePrebuilt.apk;PRESIGNED

# Carrier
product/etc/permissions/com.android.sdm.plugins.connmo.xml
product/etc/permissions/com.android.sdm.plugins.dcmo.xml
-product/priv-app/CarrierServices/CarrierServices.apk;PRESIGNED
<<<<<<< HEAD
-product/priv-app/CarrierWifi/CarrierWifi.apk;PRESIGNED
-product/priv-app/ConnMO/ConnMO.apk
-product/priv-app/DCMO/DCMO.apk
-product/priv-app/OdadPrebuilt/OdadPrebuilt.apk;PRESIGNED
-product/priv-app/TetheringEntitlement/TetheringEntitlement.apk;PRESIGNED
system_ext/framework/RadioConfigLib.jar
-system_ext/priv-app/CarrierSetup/CarrierSetup.apk;PRESIGNED
=======
>>>>>>> 4e17c28 (gms: Debloat gapps)

# Calculator
-product/app/CalculatorGooglePrebuilt/CalculatorGooglePrebuilt.apk;OVERRIDES=ExactCalculator;PRESIGNED

# Calendar
-product/app/CalendarGooglePrebuilt/CalendarGooglePrebuilt.apk;OVERRIDES=Calendar2,Calendar,Etar;PRESIGNED

# Clock
-product/app/PrebuiltDeskClockGoogle/PrebuiltDeskClockGoogle.apk;OVERRIDES=DeskClock;PRESIGNED

# Device Personalization Services
-product/priv-app/DevicePersonalizationPrebuiltPixel2021/DevicePersonalizationPrebuiltPixel2021.apk;PRESIGNED
-system/priv-app/GoogleExtServices/GoogleExtServices.apk;PRESIGNED|b9db71197fecf9886207af70a2a798fde8b23c4c

# Files
-product/priv-app/FilesPrebuilt/FilesPrebuilt.apk;PRESIGNED
-system/priv-app/DocumentsUIGoogle/DocumentsUIGoogle.apk;OVERRIDES=DocumentsUI;PRESIGNED
-system_ext/priv-app/StorageManagerGoogle/StorageManagerGoogle.apk;OVERRIDES=StorageManager;PRESIGNED

# Flipendo
-system_ext/app/Flipendo/Flipendo.apk

# Game Overlay
system/etc/permissions/game-overlay.xml

# Google search
-product/priv-app/Velvet/Velvet.apk;OVERRIDES=QuickSearchBox;PRESIGNED

# Gmail
-product/app/PrebuiltGmail/PrebuiltGmail.apk;OVERRIDES=Email;PRESIGNED

# Google Play
-product/priv-app/ConfigUpdater/ConfigUpdater.apk;PRESIGNED
-product/priv-app/Phonesky/Phonesky.apk;OVERRIDES=messaging;PRESIGNED
-product/priv-app/PrebuiltGmsCore/PrebuiltGmsCoreSc.apk:product/priv-app/PrebuiltGmsCore/PrebuiltGmsCore.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_AdsDynamite.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_CronetDynamite.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_DynamiteLoader.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_DynamiteModulesA.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_DynamiteModulesC.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_GoogleCertificates.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_MapsDynamite.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_MeasurementDynamite.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/m/independent/AndroidPlatformServices.apk;PRESIGNED
-system/app/GoogleExtShared/GoogleExtShared.apk;OVERRIDES=ExtShared;PRESIGNED
-system_ext/priv-app/GoogleServicesFramework/GoogleServicesFramework.apk;PRESIGNED

# Intelligence
-product/priv-app/DeviceIntelligenceNetworkPrebuilt/DeviceIntelligenceNetworkPrebuilt.apk
-product/priv-app/SettingsIntelligenceGooglePrebuilt/SettingsIntelligenceGooglePrebuilt.apk;OVERRIDES=SettingsIntelligence;PRESIGNED

# Keyboard
-product/app/LatinIMEGooglePrebuilt/LatinIMEGooglePrebuilt.apk;OVERRIDES=LatinIME;PRESIGNED
product/usr/share/ime/google/d3_lms/ko_2018030706.zip
product/usr/share/ime/google/d3_lms/mozc.data
product/usr/share/ime/google/d3_lms/zh_CN_2018030706.zip

# Launcher
-system_ext/priv-app/NexusLauncherRelease/NexusLauncherRelease.apk;apkOVERRIDES=Launcher3,Launcher3QuickStep,TrebuchetQuickStep;PRESIGNED

# Location
-product/app/LocationHistoryPrebuilt/LocationHistoryPrebuilt.apk;PRESIGNED
-product/app/Maps/Maps.apk;PRESIGNED

# ModuleMetadata
-product/app/ModuleMetadataGoogle/ModuleMetadataGoogle.apk

# Offline VoiceRecognition Models
product/usr/srec/en-US/acousticmodel/MARBLE_DICTATION_EP.endpointer_portable_lstm_mean_stddev
product/usr/srec/en-US/acousticmodel/MARBLE_DICTATION_EP.endpointer_portable_lstm_model
product/usr/srec/en-US/acousticmodel/MARBLE_VOICE_ACTIONS_EP.endpointer_portable_lstm_mean_stddev
product/usr/srec/en-US/acousticmodel/MARBLE_VOICE_ACTIONS_EP.endpointer_portable_lstm_model
product/usr/srec/en-US/configs/ONDEVICE_MEDIUM_CONTINUOUS.config
product/usr/srec/en-US/configs/ONDEVICE_MEDIUM_SHORT.config
product/usr/srec/en-US/configs/ONDEVICE_MEDIUM_SHORT_compiler.config
product/usr/srec/en-US/context_prebuilt/apps.txt
product/usr/srec/en-US/context_prebuilt/contacts.txt
product/usr/srec/en-US/context_prebuilt/en-US_android-auto_car_automation.action.union_STD_FST.fst
product/usr/srec/en-US/context_prebuilt/en-US_android-auto_manual_fixes_STD_FST.fst
product/usr/srec/en-US/context_prebuilt/en-US_android-auto_top_radio_station_frequencies_STD_FST.fst
product/usr/srec/en-US/context_prebuilt/songs.txt
product/usr/srec/en-US/denorm/embedded_class_denorm.mfar
product/usr/srec/en-US/denorm/embedded_covid_19.mfar
product/usr/srec/en-US/denorm/embedded_fix_ampm.mfar
product/usr/srec/en-US/denorm/embedded_normalizer.mfar
product/usr/srec/en-US/denorm/embedded_replace_annotated_punct_words_dash.mfar
product/usr/srec/en-US/denorm/porn_normalizer_on_device.mfar
product/usr/srec/en-US/endtoendmodel/marble_rnnt_dictation_frontend_params.mean_stddev
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model-encoder.part_0.tflite
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model-encoder.part_1.tflite
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model-rnnt.decoder.tflite
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model-rnnt.joint.tflite
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model.syms.compact
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model.word_classifier
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model.wpm.portable
product/usr/srec/en-US/endtoendmodel/marble_rnnt_voice_actions_frontend_params.mean_stddev
product/usr/srec/en-US/magic_mic/MARBLE_V2_acoustic_meanstddev_vector
product/usr/srec/en-US/magic_mic/MARBLE_V2_acoustic_model.int8.tflite
product/usr/srec/en-US/magic_mic/MARBLE_V2_model.int8.tflite
product/usr/srec/en-US/magic_mic/MARBLE_V2_vocabulary.syms
product/usr/srec/en-US/voice_match/MARBLE_speakerid.tflite
product/usr/srec/en-US/SODA_punctuation_config.pb
product/usr/srec/en-US/SODA_punctuation_model.tflite
product/usr/srec/en-US/config.pumpkin
product/usr/srec/en-US/g2p
product/usr/srec/en-US/g2p.syms
product/usr/srec/en-US/g2p_phonemes.syms
product/usr/srec/en-US/hotword.data
product/usr/srec/en-US/metadata
product/usr/srec/en-US/monastery_config.pumpkin
product/usr/srec/en-US/offline_action_data.pb
product/usr/srec/en-US/pumpkin.mmap
product/usr/srec/en-US/semantics.pumpkin

# Overlays
-product/overlay/BuiltInPrintService__auto_generated_rro_product.apk:product/overlay/PixelBuiltInPrintService.apk
-product/overlay/CaptivePortalLoginOverlay/CaptivePortalLoginOverlay.apk:product/overlay/CaptivePortalLoginOverlay.apk
-product/overlay/CellBroadcastReceiverOverlay/CellBroadcastReceiverOverlay.apk:product/overlay/CellBroadcastReceiverOverlay.apk
-product/overlay/CellBroadcastServiceOverlay/CellBroadcastServiceOverlay.apk:product/overlay/CellBroadcastServiceOverlay.apk
-product/overlay/ContactsProvider__auto_generated_rro_product.apk:product/overlay/PixelContactsProvider.apk
-product/overlay/GoogleConfigOverlay.apk
-product/overlay/GooglePermissionControllerOverlay.apk
-product/overlay/GoogleWebViewOverlay.apk
-product/overlay/ManagedProvisioningPixelOverlay.apk
-product/overlay/NetworkStackOverlay.apk
-product/overlay/PixelConfigOverlay2018.apk
-product/overlay/PixelConfigOverlay2019.apk
-product/overlay/PixelConfigOverlay2019Midyear.apk
-product/overlay/PixelConfigOverlay2021.apk
-product/overlay/PixelConfigOverlayCommon.apk
-product/overlay/PixelConnectivityOverlay2021.apk
-product/overlay/PixelSetupWizardOverlay.apk
-product/overlay/PixelSetupWizardOverlay2019.apk
-product/overlay/PixelSetupWizard__auto_generated_rro_product.apk:product/overlay/PixelSetupWizard_rro.apk
-product/overlay/PixelTetheringOverlay2021.apk
-product/overlay/SettingsGoogle__auto_generated_rro_product.apk:product/overlay/PixelSettingsGoogle.apk
-product/overlay/SettingsProvider__auto_generated_rro_product.apk:product/overlay/PixelSettingsProvider.apk
-product/overlay/SystemUIGXOverlay.apk
-product/overlay/SystemUIGoogle__auto_generated_rro_product.apk:product/overlay/PixelSystemUIGoogle.apk
-product/overlay/TeleService__auto_generated_rro_product.apk:product/overlay/PixelTeleService.apk
-product/overlay/Telecom__auto_generated_rro_product.apk:product/overlay/PixelTelecom.apk
-product/overlay/framework-res__auto_generated_rro_product.apk:product/overlay/Pixelframework-res.apk

<<<<<<< HEAD
# NgaResources
-product/app/NgaResources/NgaResources.apk;PRESIGNED
product/etc/sysconfig/nga.xml

# PackageInstaller
-system/priv-app/GooglePackageInstaller/GooglePackageInstaller.apk;OVERRIDES=PackageInstaller

=======
>>>>>>> 5b81ccc (gms: Remove GooglePackageInstaller)
# Permissions
product/etc/default-permissions/default-permissions.xml
product/etc/permissions/com.google.android.dialer.support.xml
product/etc/permissions/privapp-permissions-google-p.xml
product/etc/permissions/split-permissions-google.xml
system/etc/permissions/privapp-permissions-google.xml
system_ext/etc/permissions/privapp-permissions-google-se.xml

# Phone
-product/app/GoogleContacts/GoogleContacts.apk;OVERRIDES=Contacts,Contacts2;PRESIGNED
-product/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk;PRESIGNED
-product/framework/com.google.android.dialer.support.jar
-product/priv-app/GoogleDialer/GoogleDialer.apk;OVERRIDES=Dialer;PRESIGNED
-product/priv-app/PrebuiltBugle/PrebuiltBugle.apk;PRESIGNED

# Photo
-product/app/MarkupGoogle/MarkupGoogle.apk;PRESIGNED
-product/app/Photos/Photos.apk;OVERRIDES=Gallery2;PRESIGNED

# Print
-system/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk;OVERRIDES=PrintRecommendationService;PRESIGNED

# SafetyHub
-product/priv-app/SafetyHubPrebuilt/SafetyHubPrebuilt.apk;OVERRIDES=EmergencyInfo;PRESIGNED
-system_ext/app/EmergencyInfoGoogleNoUi/EmergencyInfoGoogleNoUi.apk

# Security
-product/app/DevicePolicyPrebuilt/DevicePolicyPrebuilt.apk;PRESIGNED
product/etc/security/fsverity/gms_fsverity_cert.der
product/etc/security/fsverity/play_store_fsi_cert.der
-product/priv-app/SecurityHubPrebuilt/SecurityHubPrebuilt.apk;PRESIGNED

# Setup
-product/priv-app/GoogleOneTimeInitializer/GoogleOneTimeInitializer.apk;OVERRIDES=OneTimeInitializer;PRESIGNED
-product/priv-app/GoogleRestorePrebuilt/GoogleRestorePrebuilt.apk;PRESIGNED
-product/priv-app/PartnerSetupPrebuilt/PartnerSetupPrebuilt.apk;PRESIGNED
-product/priv-app/SetupWizardPrebuilt/SetupWizardPrebuilt.apk;OVERRIDES=Provision;PRESIGNED
-system_ext/priv-app/PixelSetupWizard/PixelSetupWizard.apk;PRESIGNED

# Sound
-product/app/SoundPickerPrebuilt/SoundPickerPrebuilt.apk;PRESIGNED

# Styles & Wallpapers
-product/app/MicropaperPrebuilt/MicropaperPrebuilt.apk;PRESIGNED
-product/app/PixelThemesStub/PixelThemesStub.apk;PRESIGNED
-product/app/PixelWallpapers2021/PixelWallpapers2021.apk;PRESIGNED
-product/priv-app/PixelLiveWallpaperPrebuilt/PixelLiveWallpaperPrebuilt.apk;PRESIGNED
-system_ext/priv-app/WallpaperPickerGoogleRelease/WallpaperPickerGoogleRelease.apk;OVERRIDES=ThemePicker,WallpaperPicker,WallpaperPicker2,WallpaperCropper;PRESIGNED

# Sysconfig
product/etc/preferred-apps/google.xml
product/etc/sysconfig/google-hiddenapi-package-whitelist.xml
product/etc/sysconfig/google-staged-installer-whitelist.xml
product/etc/sysconfig/google.xml
product/etc/sysconfig/google_build.xml
product/etc/sysconfig/nexus.xml
product/etc/sysconfig/pixel_2016_exclusive.xml|f92443f76f9af566813e08495f00c37a882af824
product/etc/sysconfig/nga.xml
product/etc/sysconfig/pixel_experience_2017.xml
product/etc/sysconfig/pixel_experience_2018.xml
product/etc/sysconfig/pixel_experience_2019.xml
product/etc/sysconfig/pixel_experience_2019_midyear.xml
product/etc/sysconfig/pixel_experience_2020.xml
product/etc/sysconfig/pixel_experience_2020_midyear.xml
product/etc/sysconfig/pixel_experience_2021.xml
product/etc/sysconfig/pixel_experience_2021_midyear.xml
product/etc/sysconfig/preinstalled-packages-product-pixel-2017-and-newer.xml

# Tags
-system/priv-app/TagGoogle/TagGoogle.apk;OVERRIDES=Tag;PRESIGNED

# Turbo
-product/priv-app/TurboPrebuilt/TurboPrebuilt.apk;PRESIGNED
-system_ext/priv-app/TurboAdapter/TurboAdapter.apk

# Voice
-product/app/GoogleTTS/GoogleTTS.apk;OVERRIDES=PicoTts;PRESIGNED
-product/app/talkback/talkback.apk;PRESIGNED
-product/priv-app/RecorderPrebuilt/RecorderPrebuilt.apk;OVERRIDES=Recorder;PRESIGNED

# Wellbeing
-product/priv-app/WellbeingPrebuilt/WellbeingPrebuilt.apk;PRESIGNED
